import sys
import xml.etree.ElementTree as ET

xml_file = sys.argv[1]
fib_type = sys.argv[2]
tree = ET.parse(xml_file)
root = tree.getroot()
if fib_type == 'FIB':
  for response_element in root.findall('.//resprocessing[@scoremodel]'):
    # Add tag:
    # <setvar variablename="EvaluationType" action="Set">EXACT</setvar>
    # to exact_element
    exact_element = response_element.find('./respcondition[1]')
    setvar = ET.SubElement(exact_element, 'setvar')
    setvar.set('variablename','EvaluationType')
    setvar.set('action','Set')
    setvar.text = 'EXACT'
    # Add tag:
    # <setvar variablename="EvaluationType" action="Set">MATCHES</setvar>
    # to pattern_element
    pattern_element = response_element.find('./respcondition[2]')
    setvar = ET.SubElement(pattern_element, 'setvar')
    setvar.set('variablename','EvaluationType')
    setvar.set('action','Set')
    setvar.text = 'MATCHES'
else:
  for varequal in root.findall('.//or/varequal'):
    if '\\s*' in varequal.text:
      varequal.set('setmatch','Matches')

tree.write(xml_file,encoding='utf-8',xml_declaration=True)
