usage() {
  echo "Usage: $0 /path/to/ublearns_export.zip problem_type"
  echo "problem_type:\n\tMA - multiple answer\n\tMC - multiple choice\n\tFIB - fill in the blank\n\tFIB_PLUS - fill in the blank with multiple answers\n\tJUMBLE - jumbled sentece"
  exit 1
}

if [ "$#" -ne "2" ]; then
  usage
fi
TYPES_STRING="['MA','MC','FIB','FIB_PLUS','JUMBLE']"
VALID_TYPE=`python3 -c "print('$2' in $TYPES_STRING)"`
TYPE_MC_MA=0
TYPE_FIB=0
TYPE_JUMBLE=0
if [ "$VALID_TYPE" = "False" ]; then
  echo "Invalid value given for problem_type."
  usage
else
  IS_MC_MA=`python3 -c "print($TYPES_STRING.index('$2') <= 1)"`
  if [ "$IS_MC_MA" = "True" ]; then
    TYPE_MC_MA=1
  else
    IS_FIB=`python3 -c "print($TYPES_STRING.index('$2') <= 3)"`
    if [ "$IS_FIB" = "True" ]; then
      TYPE_FIB=1
    else
      IS_JUMBLE==`python3 -c "print($TYPES_STRING.index('$2') == 4)"`
      if [ "$IS_JUMBLE" = "True" ]; then
        TYPE_JUMBLE=1
      fi
    fi
  fi
fi

ZIPFILE="$1"
BASENAME=$(basename -- "$ZIPFILE")
ORIGINALPATH="${ZIPFILE%$BASENAME}"
FILEBASE="${BASENAME%.zip}"
if [ "$BASENAME" = "$FILEBASE" ]; then
  echo "Argument must be a zip file."
  usage
fi

WORKING_DIR=`pwd`
unzip -d "$WORKING_DIR/$FILEBASE" "$ZIPFILE"

# Enable partial credit.
perl -pi -e 's/<bbmd_partialcredit><\/bbmd_partialcredit>/<bbmd_partialcredit>true<\/bbmd_partialcredit>/g' "$WORKING_DIR/$FILEBASE/res00001.dat"
# Handle partial credit and random order for multiple choice/multiple answer.
if [ "$TYPE_MC_MA" = "1" ]; then 
  perl -pi -e 's/<bbmd_negative_points_ind>N<\/bbmd_negative_points_ind>/<bbmd_negative_points_ind>Q<\/bbmd_negative_points_ind>/g' "$WORKING_DIR/$FILEBASE/res00001.dat"
  # Enable random ordering.
  perl -pi -e 's/render_choice shuffle="No"/render_choice shuffle="Yes"/g' "$WORKING_DIR/$FILEBASE/res00001.dat"
  # Set individual values for partial credit.
  python3 "$WORKING_DIR/set_partial.py" "$WORKING_DIR/$FILEBASE/res00001.dat"
else
  if [ "$TYPE_FIB" = "1" ]; then
    echo "IS FIB"
    # Set regex pattern match for fill-in-the-blank questions.
    python3 "$WORKING_DIR/set_pattern_match.py" "$WORKING_DIR/$FILEBASE/res00001.dat" "$2"
  fi
fi
# Rezip
(cd "$WORKING_DIR/$FILEBASE" && zip -r - .) > "$WORKING_DIR/$FILEBASE-updated.zip"
# Copy to original dir
cp "$WORKING_DIR/$FILEBASE-updated.zip" "$ORIGINALPATH"
# Cleanup
rm -rf "$WORKING_DIR/$FILEBASE/"
rm -f "$WORKING_DIR/$FILEBASE-updated.zip"
