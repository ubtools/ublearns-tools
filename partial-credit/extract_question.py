import sys
import csv
import re

input_csv_file = sys.argv[1]
csv_col = int(sys.argv[2])
output_directory = sys.argv[3]
output_suffix = sys.argv[4]

output_data = {}
with open(input_csv_file,encoding='utf-8') as csv_file:
  spamreader = csv.reader(csv_file, delimiter=',', quotechar='"')
  header = next(spamreader,'')
  for row in spamreader:
    # UBIT : Question text
    output_data[row[0]] = row[csv_col]

for ubit, question_text in output_data.items():
  with open(output_directory + f'{ubit}-{output_suffix}', 'w',encoding='utf-8') as output_csv_file:
    processed_output = re.sub('<br\s*/?>','\n',question_text)
    processed_output = re.sub('<.+?>','',processed_output)
    processed_output = processed_output.replace('&quot;','"')
    output_csv_file.write(processed_output)
