import sys
import xml.etree.ElementTree as ET

correct_point_lists = [[]]
for c in range(1,9+1):
  divided_correct_percent = str(100 / c)
  if divided_correct_percent[-2:] == '.0':
    divided_correct_percent = divided_correct_percent[:divided_correct_percent.find('.')]
    points_list = [divided_correct_percent] * c
  else:
    divided_correct_percent = str(divided_correct_percent)[:divided_correct_percent.find('.')+11]
    points_list = [1.0] * c
    last_digit_increased = str(int(divided_correct_percent[-1])+1)
    for i in range(0,100 % c):
      points_list[i] = divided_correct_percent[:-1]+last_digit_increased
    for i in range(100%c, c):
      points_list[i] = divided_correct_percent
    
  #print(divided_correct_percent,points_list)
  correct_point_lists.append(points_list)

incorrect_point_lists = [['-' + x for x in l] for l in correct_point_lists]  
#print(incorrect_point_lists)

    

xml_file = sys.argv[1]
#print(xml_file)
tree = ET.parse(xml_file)
root = tree.getroot()
for response_element in root.findall('.//respcondition[@title="correct"]/..'):
  problem_element = response_element.findall('./respcondition[@title="correct"]')[0]
  correct = list(map(lambda e: e.text.strip(), problem_element.findall('.//conditionvar/and/varequal')))
  #print('Correct IDs:',correct)
  incorrect = list(map(lambda e: e.text.strip(), problem_element.findall('.//conditionvar/and/not/varequal')))
  #print('Incorrect IDs:',incorrect)
  vareq = [elem for id in correct for elem in response_element.findall('.//varequal[@respident="'+id+'"]/../..')]
  #print('Correct VAREQ:',vareq)
  vareq = [elem for id in incorrect for elem in response_element.findall('.//varequal[@respident="'+id+'"]/../..')]
  #print('Incorrect VAREQ:',vareq)
  correct_partial_credit = [(id,credit,credit.text.strip()) for id in correct for credit in response_element.findall('.//varequal[@respident="' + id + '"]/../../setvar')]
  correct_credit_list = correct_point_lists[len(correct_partial_credit)]
  for index,(id, element, tag) in enumerate(correct_partial_credit):
    element.text = correct_credit_list[index]
  #print('Correct partial credit:',partial_credit)
  incorrect_partial_credit = [(id,credit,credit.text.strip()) for id in incorrect for credit in response_element.findall('.//varequal[@respident="' + id + '"]/../../setvar')]
  incorrect_credit = incorrect_point_lists[len(incorrect_partial_credit)][0]
  for id, element, tag in incorrect_partial_credit:
    element.text = incorrect_credit
  #print('Incorrect partial credit:',partial_credit)

tree.write(xml_file,encoding='utf-8',xml_declaration=True)
