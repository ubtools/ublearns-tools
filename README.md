# README #

Please note that these scripts may be fickle and worked at some time in the
past to some degree of success. Use at your own risk.

### Partial Credit Scripts ###

The folder partial-credit contains a script to (correctly?) manipulate an
exported UB Learns pool of questions to set partial credit, random ordering,
etc. Please be careful to double-check that things re-import correctly.

Note:

 * All questions in the pool must be the same type (untested otherwise).
 * For fill-in-the-blank, the default behavior is to have the format of two
  correct answers per problem. The first is an exact answer. The second is a
  pattern match. The script will set the second to pattern match for each
  problem.
    * If you do not use pattern matching, please comment this out in the script.

To use:

 1. Export a pool using "Export". Do not use "Export as QTI 2.1 Package".
 2. Run ublearns\_ma\_partial\_update.sh with the correct question type.
    * FIB - fill in the blank
    * FIB\_PLUS - fill in the blank with multiple blanks
    * MA - multiple answer questions
    * MC - multiple choice questions
 3. Import the updated .zip file (created in the directory of the original).

### Adaptive Release Script ###

To set adaptive release times:

 1. Save the text file containing the javascript.
 2. Update the groups array with the appropriate info (name, date, start, end).
 3. Open UB Learns.
 4. Open the Developer Console.
 5. Go to your item's adaptive release advanced page.
 6. Copy+paste the javascript into the console and run.

Note -- be sure you keep the window in focus and do not click anything. If the
window loses focus, UB Learns will delay loading.

### Collect Names Script ###

This script is used to collect the list of names on submissions for a specific
question under UB Learns grading (when grading by question). This can be copied
and pasted directly into the developer console.

After running the script, you can get the output via `obj.str` within the
console. Unfortunately you will have to trim the empty spaces/lines manually.

### Who do I talk to? ###

* Contact Andrew Hughes (ahughes6@buffalo.edu) with questions or issues.
